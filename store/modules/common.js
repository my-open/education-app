export default {
	
  namespaced: true,
  state: {
     host: 'http://127.0.0.1', //'http://education.free.idcfengye.com',//'http://127.0.0.1', //,
	 fileHost: 'http://127.0.0.1/uploads'
  },
  
  getters: {
	  
	   host: state => {
	      return state.host
	   },
	   
	   fileHost: state => {
	      return state.fileHost
	   },
  },
  
  mutations: {

  }
}
